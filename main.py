"""main skriptas"""
import graph
import dijkstra
import re


def from_file_to_graph(filename):
    """main funkcija"""
    try:
        input_file = open(filename, "r")
    except IOError as error:
        print("Nepavyko atidaryti failo.\n{0} {1}".format(error.errno,
                                                          error.strerror))

    line = input_file.readline()
    pattern = r"\A(?P<number_of_lines>\d+) (?P<from>\w+) (?P<to>\w+)\s*\Z"
    match = re.match(pattern, line)

    if match is None:
        input_file.close()
        raise Exception("Netinkama pirmoji duomenų failo eilutė.")

    groupdict = match.groupdict()
    number_of_lines = int(groupdict["number_of_lines"])
    from_city = groupdict["from"]
    to_city = groupdict["to"]

    graph_obj = graph.Graph()
    try:
        lines = list(input_file)[:number_of_lines]
    except KeyError:
        raise Exception("Per mažai eilučių."
                        " Turi būti bent {0}.".format(number_of_lines))
    finally:
        input_file.close()

    for line in lines:
        pattern = r"\A(?P<from>\w+) (?P<to>\w+) (?P<name_of_owner>\w+)"\
                  + r" (?P<weight>\d+(\.\d{1,2})?)\s*\Z"
        match = re.match(pattern, line)
        if match is None:
            input_file.close()
            raise Exception("Netinkama eilutė '{0}'.".format(line))
        groupdict = match.groupdict()
        graph_obj.add_edge(groupdict["from"], groupdict["to"],
                           groupdict["name_of_owner"], groupdict["weight"])
        graph_obj.add_edge(groupdict["to"], groupdict["from"],
                           groupdict["name_of_owner"], groupdict["weight"])
    return (graph_obj, from_city, to_city)


def main():
    try:
        graph_obj, from_city, to_city = from_file_to_graph("duom.txt")
    except:
        exit(1)
    min_distance, previous = dijkstra.Dijkstra_compute_shortest_path(from_city,
                                                                     graph_obj)
    path = dijkstra.Dijkstra_get_shortest_path(to_city, previous)
    print("Trumpiausio maršruto kaina {0}.".format(min_distance[to_city]))
    print("Šis maršrutas yra:")
    for vertex in path:
        print("\t{0}".format(vertex))

if __name__ == "__main__":
    main()
