from collections import deque
from collections import defaultdict
from decimal import Decimal


def Dijkstra_compute_shortest_path(from_city, graph):
    min_distance = defaultdict(lambda: Decimal(float("inf")))
    min_distance[from_city] = 0
    edge_queue = deque()
    previous = defaultdict(lambda: "")
    edge_queue.append((min_distance[from_city], from_city))
    while len(edge_queue) != 0:
        edge = edge_queue.popleft()
        dist = edge[0]
        u = edge[1]
        neighbours = graph.neighbours(u)
        for neighbour in neighbours:
            v = neighbour
            weight = graph.edge_at(u, v).weight
            if graph.edge_at(u, v).name_of_owner == from_city:
                weight /= 2
            distance_through_u = dist + weight
            if distance_through_u < min_distance[v]:
                try:
                    edge_queue.remove((min_distance[v], v))
                except ValueError:
                    pass
                min_distance[v] = distance_through_u
                previous[v] = u
                edge_queue.append((min_distance[v], v))
    return (min_distance, previous)


def Dijkstra_get_shortest_path(edge, previous):
    path = deque()
    if previous[edge] != "":
        while edge != "":
                path.appendleft(edge)
                edge = previous[edge]
    return path
