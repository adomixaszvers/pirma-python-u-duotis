from decimal import Decimal


class Edge(object):
    def __init__(self, name_of_owner, weight=float("inf")):
        self.name_of_owner = name_of_owner
        self.weight = Decimal(weight)


class Graph(object):
    def __init__(self):
        self.__vertices = set()
        self.__graph = dict()

    def vertices(self):
        return self.__vertices

    def add_vertex(self, name):
        self.__vertices.add(name)

    def add_edge(self, from_city, to_city, name_of_owner, weight):
        self.add_vertex(from_city)
        self.add_vertex(to_city)
        edge = Edge(name_of_owner, weight)
        try:
            self.__graph[from_city].update({to_city: edge})
        except KeyError:
            self.__graph[from_city] = {to_city: edge}

    def neighbours(self, vertex):
        try:
            return self.__graph[vertex].keys()
        except KeyError:
            return {}

    def edge_at(self, from_city, to_city):
        try:
            edge = self.__graph[from_city][to_city]
        except KeyError:
            edge = Edge(None, float("inf"))
        return edge
